# <:package_name>

![License](https://img.shields.io/badge/license-MIT-green?style=plastic) ![GitLab](https://img.shields.io/badge/-GitLab-FCA121?style=plastic&logo=gitlab) ![Php](https://img.shields.io/badge/-Php-394989?style=plastic&logo=php)

Modelo de biblioteca composer que utilizo para projetos pessoais

## Estrutura

```text
<:package_name>
├── src
│   └── exemplo
│       ├── exemplo 1
│       ├── exemplo 2
│       └── exemplo 3
├── tests
├── LICENSE.md
└── README.md
```


## Requisitos

- PHP +7.3.x
- Composer +2.x
- Terminal ex. Git Bash

## Instalação

Abra seu terminal e digite

```bash
$ git clone https://gitlab.com/ricardo-melo-martins/<:package_name>.git
$ cd <:package_name>
```

Via Composer

```sh
$ composer config repositories.<:package_name> vcs https://gitlab.com/ricardo-melo-martins/<:package_name>
```

Em seguida, instale o pacote:

```bash
$ composer require ricardo-melo-martins/<:package_name>:dev-master
```

## Exemplo de uso

``` php
<?php

$out = new RMM\Stdlib();
echo $out->echoPhrase('Hello World!');

```

<!-- ## Change log -->

## Teste

``` bash
$ composer test
```


## Sugestões ou Bugs

Se encontrou algum problema ou tem alguma sugestão por favor utilize a seção de Issues [aqui](https://github.com/Yilber/readme-boilerplate/issues) .



## Licença

É gratuito sob licença MIT.

Por favor veja [aqui](LICENSE.md) para mais informações.

Criado com :heart: por [Ricardo Melo Martins][link-author].

> 
[ico-php]:  	https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white
[ico-gitlab]: https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white
[ico-version]: https://img.shields.io/packagist/v/ricardo-melo-martins/rmm-library-php-stdlib.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://gitlab.com/ricardo-melo-martins
