<?php

// phpcs:ignore

namespace RMM\Example;

/**
 * Classe de Somas
 *
 * @category Description
 * @package  Example
 * @author   Ricardo Melo Martins
 * @license  MIT https://gitlab.com/ricardo-melo-martins/rmm-composer-boilerplate/LICENSE.md
 * @link     https://gitlab.com/ricardo-melo-martins/rmm-composer-boilerplate
 */
class Soma
{
    /**
     * Função para somar
     *
     * @param integer $n numero para somar
     *
     * @return integer
     */
    public static function sum(int $n): int
    {
        return $n * ($n + 1) / 2;
    }
}
