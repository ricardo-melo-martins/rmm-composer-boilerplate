<?php
// phpcs:ignore
declare(strict_types=1);

namespace RMM\Example\Test;

use PHPUnit\Framework\TestCase;

use RMM\Example\Soma;

/**
 * Classe de Teste para Somas
 *
 * @category Description
 * @package  Example/Tests
 * @author   Ricardo Melo Martins
 * @license  MIT https://gitlab.com/ricardo-melo-martins/rmm-composer-boilerplate/LICENSE.md
 * @link     https://gitlab.com/ricardo-melo-martins/rmm-composer-boilerplate
 */
class SomaTest extends TestCase
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function testIsSumCorrect(): void
    {
        $result = Soma::sum(5);

        $this->assertEquals($result, 15);
    }

    /**
     * Teste de Vazio
     *
     * @return array
     *
     * @depends testIsSumCorrect
     */
    public function testIsEmptyCorrect ( )
    {
        $stack = [];
        $this->assertEmpty ($stack);


        return $stack;
    }

    /**
     * Teste de Push
     *
     * @param array $stack array dados vazio
     *
     * @return array
     *
     * @depends testIsEmptyCorrect
     */
    public function testIsPushCorrect(array $stack)
    {
        array_push($stack, "foo");
        $this->assertEquals("foo", $stack[count($stack) - 1]);
        $this->assertNotEmpty($stack);

        return $stack;
    }

    /**
     * Teste de Pop
     *
     * @param array $stack array dados vazio
     *
     * @return void
     *
     * @depends testIsPushCorrect
     */
    public function testIsPopCorrect(array $stack)
    {
        $this->assertEquals("foo", array_pop($stack));
        $this->assertEmpty($stack);
    }
}
